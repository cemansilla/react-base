// Dependencies
import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';

// Components
import SignIn from './SignIn';

// Shared
import Header from '../shared/components/layout/Header';
import Content from '../shared/components/layout/Content';
import Footer from '../shared/components/layout/Footer';
import '../shared/styles/app.scss';

function App() {
  return (
    <div>
      <CssBaseline />

      <Content>
        <SignIn></SignIn>
      </Content>

    </div>
  );
}

export default App;
