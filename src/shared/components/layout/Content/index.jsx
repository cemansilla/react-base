import React from 'react';
import { element } from 'prop-types';

const Content = (props) => (
  <div className="Content">
    { props.children }
  </div>
);

Content.propTypes = {
  children: element
}

export default Content;