import React from 'react';

import logo from '../../../images/logo.png';

const Header = () => (
  <header className="Header">
    <img src={ logo } className="App-Logo" alt="logo" />
  </header>
);

export default Header;